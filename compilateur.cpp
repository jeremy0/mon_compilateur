//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current;// Current car	
char nextcar;//  Next car		

void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	current = nextcar;
	while(cin.get(nextcar) && (current==' '||current=='\t'||current=='\n'))
	{}	//nextcar=std::cin.peek();  	
	
}


void Error(string s){
	cerr<< s << endl;
	exit(-1);
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
//<expression> ::= <ArithmeticExpression> | <ArithmeticExpression> <AdditiveOperator> <ArithmeticExpression> 
//<AdditiveOperator> ::= '=' | '<>' | '<' | '<=' | '>=' | '>'  

	
void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}
void RelationelOperator(void){
	if(current=='=')
	{
		ReadChar();
	}
	else if(current=='<')
	{
		if(nextcar=='>')
		{
			ReadChar();
		}
		else if(nextcar=='=')
		{
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='>')
	{
		if(nextcar=='=')
		{
			ReadChar();
		}
		ReadChar();
	}
	else
	{
		Error("Opérateur Comparatif attendu : = ou < ou >");
	}
}
		
void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()
void RelationelOperator(void);

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
		{
			Error("')' était attendu");		// ")" expected
		}
		else
		{
			ReadChar();
		}
	}
	else if(current == '=' || current == '<' || current == '>')
	{
		RelationelOperator();
		ReadChar();
	}
	else 
		if (current>='0' && current <='9')
		{
			Digit();
		}
	    else
	    {
			Error("'(' ou chiffre attendu");
		}	
}

void ArithmeticExpression(void){

}

void Expression(void)
{
	char re;
	char re2;
	ArithmeticExpression();
	if(current == '=' || current == '<' || current == '>')
	{
		re = current;
		re2 = nextcar;
		ArithmeticExpression();
		ArithmeticExpression();
		cout<<"\t pop %rbx"<<endl;
		cout<<"\t pop %rax"<<endl;
		cout<<"\t cmpq %rbx %rax"<<endl;
		if(re == '=')
		{
			cout<<"\t ja Vrai"<<endl;
		}
		else if(re == '>')
		{
			if(re2 == '=')
			{
				cout<<"\t jae Vrai"<<endl;
			}
			else
			{
				cout<<"\t ja Vrai"<<endl;
			}
		}
		else if(re == '<')
		{
			if(re2 == '=')
			{
				cout<<"\t jbe Vrai"<<endl;
			}
			else if(re2 == '>')
			{
				cout<<"\t jne Vrai"<<endl;
			}
			else
			{
				cout<<"\t jb Vrai"<<endl;
			}
		}
		cout<<"Faux"<<endl;
		cout<<"\t push $0"<<endl;
		cout<<"\t jmp Suite"<<endl;
		cout<<"Vrai"<<endl;
		cout<<"\t push 0xFFFFFFFFFFFFFFFF"<<endl;
		cout<<"Suite"<<endl;
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ReadChar();
	ArithmeticExpression();
	if(current == '=' || current == '<' || current == '>')
	{
		RelationelOperator();
	}
	ReadChar();
	// Trailer for the gcc assembler / linker

		
			





